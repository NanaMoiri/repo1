﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculadora1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
   
    {
        double num =0;
        string signo = "";

        public MainWindow()
        {
            InitializeComponent();



           //var num = new int[10];
           //num[0] = 0;
           //num[1] = 1;
           //num[2] = 2;
           //num[3] = 3;
           //num[4] = 4;
           //num[5] = 5;
           //num[6] = 6;
           //num[7] = 7;
           //num[8] = 8;
           //num[9] = 9;
    
        }
  
        private void btn0_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "0";
            var num = 0;



        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "1";
            var num = 1;

         
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "2";
         

        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "3";
   
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "4";

        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "5";
     
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "6";
         
        }

        private void btn7_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "7";
   
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            textBox.Text = textBox.Text + "8";
           
        }

        private void btn9_Click(object sender, RoutedEventArgs e)
        {
            
            textBox.Text = textBox.Text + "9";
           
         
        }

        private void btnce_Click(object sender, RoutedEventArgs e)
        {
            //textBox.Text = " ";

            textBox.Clear();

        }

        private void btnbor_Click(object sender, RoutedEventArgs e)
        {
            // string textBox = textBox.Trim(); (prueba no funciona)

            //char[] MyChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            textBox.Text = textBox.Text.Substring(0,textBox.Text.Length-1);

            //textBox.Text = textBox.Text.Substring(0, textBox.Length)

            //textBox.Text = textBox.Text.Length;


        }

        private void btnsum_Click(object sender, RoutedEventArgs e)
        {
            num = Convert.ToDouble(textBox.Text);
            signo = "+";
            textBox.Text = "";
        }

        private void btnrslt_Click(object sender, RoutedEventArgs e)
        {
            double num2 = Convert.ToDouble(textBox.Text);

            switch (signo) {

                case "+":
                    num = num + num2;
                    textBox.Text = Convert.ToString(num);
                    break;

                case "-":
                    num = num - num2;
                    textBox.Text = Convert.ToString(num);
                    break;
                case "*":
                    num = num * num2;
                    textBox.Text = Convert.ToString(num);
                    break;
                case "/":
                    num = num / num2;
                    textBox.Text = Convert.ToString(num);
                    break;

            }

        }

        private void btnres_Click(object sender, RoutedEventArgs e)
        {
            num = Convert.ToDouble(textBox.Text);
            signo = "-";
            textBox.Text = "";
        }

        private void btnmult_Click(object sender, RoutedEventArgs e)
        {
            num = Convert.ToDouble(textBox.Text);
            signo = "*";
            textBox.Text = "";
        }

        private void btndiv_Click(object sender, RoutedEventArgs e)
        {
            num = Convert.ToDouble(textBox.Text);
            signo = "/";
            textBox.Text = "";
        }


    }
}



